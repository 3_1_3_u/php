[![coverage report](https://gitlab.com/3_1_3_u/php/badges/master/coverage.svg)](https://gitlab.com/3_1_3_u/php/commits/master)

Example PHP project
===================

The purpose of this repository is to show how to use GitLab to do
Continuous Integration with a PHP project. It serves as a companion project for
<https://docs.gitlab.com/ce/ci/examples/php.html>.

In order to run this project just fork it on GitLab.com.
Every push will then trigger a new build on GitLab.

Source
------
This project was taken from: https://github.com/travis-ci-examples/php.